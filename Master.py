import asyncio
import time
from collections import deque

#Address dan port dimana master server 'listen' data yang masuk
server = {
    "host" : "127.0.0.1",
    "port" : 7070
}

#Address dan port dimana master akan mengirim message pada worker
worker_1 = {
    "host" : "54.234.156.181",
    "port" : 6543,
    "availability" : 1,
    "name" : "Worker-1"
}

worker_2 = {
    "host" : "52.207.247.7",
    "port" : 7080,
    "availability" : 1,
    "name" : "Worker-2"
}

workers = [worker_1, worker_2]

#deque sebagai antrian FCFS di master node
q = deque()

#Method untuk mengirim message ke worker yang dituju
async def tcp_echo_client(message, host, port):
    #establish connection dengan worker
    print('open connection with worker')
    reader, writer = await asyncio.open_connection(
        host, port)

    #Mengirim message ke worker
    print(f'Send: {message!r} to worker')
    writer.write(message.encode())
    await writer.drain()

    #Menunggu hingga menerima balasan dari worker lalu return
    data = await reader.read(100)
    print(f'Received: {data.decode()!r} from worker')

    print('Close the connection with worker')
    writer.close()
    return data

async def tcp_master_server(reader, writer):
    #Master server menunggu untuk menerima message dari client
    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info('peername')
    print("Master Server Received %r from %r" % (message, addr))

    message_split = message.split(" ")
    assigned_worker = None

    #Memeriksa message yang dikirim oleh client
    if message_split[0] == "checkavail":
        #Jika message berupa checkavail, maka akan dikirim informasi availability setiap worker
        returned_message = ""
        for worker in workers:
            if worker["availability"]==1:
                returned_message+=worker["name"]+" is available ---- "
            else :
                returned_message+=worker["name"]+" is busy ---- "
        print("Send: %r to sender" % returned_message)
        returned_message = returned_message.encode()
    else:
        #Masukkan message dalam antrian
        q.append(message)

        #assign worker untuk melakukan tugas, jika semua worker sibuk, maka menunggu hingga ada worker yang tersedia
        while True:
            for worker in workers:
                if worker["availability"] == 1:
                    worker["availability"]=0
                    assigned_worker = worker
                    break
                else:
                    print("%r is busy" % worker["name"])
            print("all workers are busy, please wait")
            if assigned_worker:
                break
            await asyncio.sleep(5)

        #Kirim informasi pada client ke worker mana message mereka akan dikirim
        message_to_send = q.popleft()
        info_to_send = "Your message will be sent to "+assigned_worker["name"]
        writer.write(info_to_send.encode())
        await writer.drain()

        #Kirim message dari client ke worker, lalu masukkan message yang direturn worker dalam variable
        print("Message %r is assigned to %r" % (message_to_send, assigned_worker["name"]))
        task = asyncio.Task(tcp_echo_client(message_to_send, assigned_worker["host"], assigned_worker["port"]))
        returned_message = await task

        #Buat worker menjadi available kembali setelah melakukan task agar bisa melakukan task lain
        print("%r is finished doing its task and is available again" % assigned_worker["name"])
        assigned_worker["availability"] = 1
        print("Send: %r to sender" % returned_message.decode())

    #Kirim message dari worker ke client
    writer.write(returned_message)
    await writer.drain()

    print("Close the master socket")
    writer.close()


loop = asyncio.get_event_loop()
coro = asyncio.start_server(tcp_master_server, server['host'], server['port'], loop=loop)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()