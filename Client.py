import asyncio
import sys

async def tcp_echo_client(message, host, port):
    #establish connection dengan master server
    reader, writer = await asyncio.open_connection(
        host, port)

    #Kirim message sesuai argumen
    print(f'Send: {message!r}')
    writer.write(message.encode())
    await writer.drain()

    #Tunggu kembalian dari server mengenai informasi worker mana yang akan digunakan untuk mengolah message
    info = await reader.read(100)
    print(f'Information received : {info.decode()!r}')

    #Tunggu informasi kembali dari master
    data = await reader.read(100)
    print(f'Received: {data.decode()!r}')

    #close connection
    print('Close the connection')
    writer.close()
    return data.decode()

async def main():
    message = ""
    if len(sys.argv)==1:
        message = "Hello World!"
    else:
        for i in range (1,len(sys.argv)):
            message+=sys.argv[i]
            if i!=len(sys.argv)-1:
                message+=" "
    await tcp_echo_client(message, "127.0.0.1", 7070)

def run(aw):
    if sys.version_info >= (3, 7):
        return asyncio.run(aw)
    # Emulate asyncio.run() on older versions
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        return loop.run_until_complete(aw)
    finally:
        loop.close()
        asyncio.set_event_loop(None)

run(main())