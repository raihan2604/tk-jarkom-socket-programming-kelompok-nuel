import asyncio
import time

def compare(x,y):
    try:
        int_x = int(x)
        int_y = int(y)
        if(int_x < int_y):
            return(f'{int_x} is Smaller than {int_y}')
        elif(int_x > int_y):
            return(f'{int_x} is Bigger than {int_y}')
        else:
            return(f'{int_x} is equal to {int_y}')

    except ValueError:
        # Handle the exception
        return('[ERROR] Please enter an integer')
def factorial(x):
    try:
        string_int = int(x)
        fact = 1
        for i in range(1, string_int + 1):
            fact = fact * i
        time.sleep(1)
        return(f"The factorial of {x} is : {fact}")
    except ValueError:
        # Handle the exception
        return('[ERROR] Please enter an integer')
def sorting(data):
    try:
        new_data = []
        data_length = len(data)
        for a in range(data_length):
            new_data.append(int(data[a]))
        # print(new_data)
        for i in range(data_length-1):
            for j in range(0, data_length-i-1):
                if new_data[j] > new_data[j+1] :
                    new_data[j], new_data[j+1] = new_data[j+1], new_data[j]
        time.sleep(60)
        return(f'Sorted list :{new_data}')

    except ValueError:
        # Handle the exception
		
        return('[ERROR] Please enter an integer')

async def handle_echo(reader, writer):
    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info('peername')
    print("Worker-1 Received %r from %r" % (message, addr))
    time.sleep(10)

    message_split = message.split()
    # Compare
    if (message_split[0] == "C"):
        hasil = compare(message_split[1], message_split[2])
        print(f'Send : {hasil}')
        writer.write(str.encode(hasil))
        
        await writer.drain()

        print("Close the client socket")
        writer.close()
    # Factorial
    elif (message_split[0] == "F"):
        hasil = factorial(message_split[1])
        print(f'Send : {hasil}')
        writer.write(str.encode(hasil))
        
        await writer.drain()

        print("Close the client socket")
        writer.close()
    # Sorting
    elif (message_split[0] == "S"):
        hasil = sorting(message_split[1:len(message_split)])
        print(f'Send : {hasil}')
        writer.write(str.encode(hasil))
        
        await writer.drain()

        print("Close the client socket")
        writer.close()
    else:
        print("Send: %r" % message)
        writer.write(data)
        await writer.drain()

        print("Close the client socket")
        writer.close()


loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_echo, '0.0.0.0', 5050, loop=loop)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()